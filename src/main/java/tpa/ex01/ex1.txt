Ülesanne 1.

Teha läbi StringCalculator-i näide.

Kirjuatada klass StringCalculator kirjutades testid enne koodi. StringCalculator-il
on meetod add(), mis võtab sisse komaga eraldatud numbrite jada ja tagastab 
nende numbrite summa.

Oodatud tulemus (sisend -> väljund):

"" -> 0
"1" -> 1
"1, 2" -> 3
null -> IllegalArgumentException

Testklassi malli leidate tpa.ex01.StringCalculatorTest klassist
