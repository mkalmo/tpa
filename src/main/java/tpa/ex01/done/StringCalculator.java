package tpa.ex01.done;

class StringCalculator {

    public int add(String string) {

        if (string == null) {
            throw new IllegalArgumentException();
        }

        if ("".equals(string)) {
            return 0;
        }

        int sum = 0;
        for (String each : string.split(",\\s*")) {
            sum += Integer.parseInt(each);
        }

        return sum;
    }


}