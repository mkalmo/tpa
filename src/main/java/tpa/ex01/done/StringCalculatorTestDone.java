package tpa.ex01.done;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class StringCalculatorTestDone {

//    "" -> 0
//    "1" -> 1
//    "1, 2" -> 3
//    null -> IllegalArgumentException

    @Test
    public void emptyStringReturnsZero() {
        StringCalculator c = new StringCalculator();

        int result = c.add("");

        assertThat(result, is(0));
    }

    @Test
    public void singleNumberReturnsItsValue() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1");

        assertThat(result, is(1));
    }

    @Test
    public void multipleNumbersReturnsTheSum() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1, 2");

        assertThat(result, is(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullThrows() {
        StringCalculator c = new StringCalculator();

        c.add(null);
    }
}
