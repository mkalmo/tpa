Ülesanne 5.

Panna tööle test ReportTest.testReportTotalIncome(). Selleks tuleb asendada
puuduv kood stub meetoditega ehk teha järgmist:

1. Kirjutada lõpuni TestableReport, mis kirjutab üle getIncomesBetween
   tagastades fikseeritud listi money objektidest.
   nt. Arrays.asList(new Money(1, "EUR"), new Money(15, "EEK"))
   Kasutada testis loodud alamklassi Report klassi asemel.


2. Kirjutada lõpuni TestableBank, mis kirjutab üle convert() meetodi
   nt. if ("EEK".equals(money.getCurrency())) {
           return new Money(money.getAmount() / 15, "EUR");
       } else {
           return money;
       }
   Kasutada testis loodud alamklassi Bank klassi asemel.

3. Muutke testi selliselt, arvutuseks kasutatavad summad oleks testis näha:
   new Money(1, "EUR"), new Money(15, "EEK")

    a. kasutage anonüümseid alamklasse
    b. pange anonüümse alamklassi loomine eraldi meetodisse,
       mis võtab summad parameetrina sisse.
