package tpa.ex05.done;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import tpa.common.Money;
import tpa.ex05.Bank;
import tpa.ex05.Report;

public class ReportTestDone {

    @Test
    public void reportsTotalIncomeVersion1() {
        // separate classes, constructor injection
        Report report = new TestableReport(new TestableBank());

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

    @Test
    public void reportsTotalIncomeVersion2() {
        // separate classes, injection with reflection
        Report report = new TestableReport();
        Inject.bean(report).with(new TestableBank());

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

    @Test
    public void reportsTotalIncomeVersion3() {
        // anonymous classes, constructor injection
        Bank bank = new Bank() {
            @Override
            public Money convert(Money money, String toCurrency) {
                if ("EEK".equals(money.getCurrency())) {
                    return new Money(money.getAmount() / 15, "EUR");
                } else {
                    return money;
                }
            }
        };

        Report report = new Report(bank) {
            @Override
            protected List<Money> getIncomesBetween(Date startDate, Date endDate) {
                return Arrays.asList(new Money(1, "EUR"), new Money(15, "EEK"));
            }
        };

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

    @Test
    public void reportsTotalIncomeVersion4() {
        // delegated class creation
        Report report = getReportWithIncomes(
                new Money(1, "EUR"), new Money(15, "EEK"));

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

    private Report getReportWithIncomes(final Money ... moneys) {
        return new Report(new TestableBank()) {
            @Override
            protected List<Money> getIncomesBetween(Date startDate, Date endDate) {
                return Arrays.asList(moneys);
            }
        };
    }
}

class TestableReport extends Report {
    public TestableReport(Bank bank) {super(bank);}
    public TestableReport() {super(null);}

    @Override
    protected List<Money> getIncomesBetween(Date startDate, Date endDate) {
        return Arrays.asList(new Money(1, "EUR"), new Money(15, "EEK"));
    }
}

class TestableBank extends Bank {

    @Override
    public Money convert(Money money, String toCurrency) {
        if ("EEK".equals(money.getCurrency())) {
            return new Money(money.getAmount() / 15, "EUR");
        } else {
            return money;
        }
    }
}
