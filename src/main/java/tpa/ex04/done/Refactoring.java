package tpa.ex04.done;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class Refactoring {

    // 1a
    public int nextCount() {
        return count++;
    }

    // 1b
    public void collectFilledOrdersTo(List<Order> o) {
        for (Order order : orders) {
            if (order.isFilled()) {
                o.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);

        printValue(calculateInvoiceTotal(invoiceRows));
    }

    private double calculateInvoiceTotal(List<InvoiceRow> invoiceRows) {
        double t = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            t += invoiceRow.getAmount();
        }
        return t;
    }

    // 2b
    public String getItemsAsHtml() {
        String retval = "";
        for (String item : Arrays.asList(item1, item2, item3, item4)) {
            retval += encloseInTag("li", item);
        }
        return encloseInTag("ul", retval);
    }

    private String encloseInTag(String tag, String content) {
        return MessageFormat.format("<{0}>{1}</{0}>", tag, content);
    }

    @Test
    public void getItemsAsHtmlTest() {
        assertThat(getItemsAsHtml(),
                   is("<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul>"));
    }

    // 3
    public boolean accessResourceAllowed(SessionData sessionData) {
        return isAdmin(sessionData)&& hasPreferredStatus(sessionData);
    }

    private boolean hasPreferredStatus(SessionData sessionData) {
        return sessionData.getStatus().equals("preferredStatusX")
            || sessionData.getStatus().equals("preferredStatusY");
    }

    private boolean isAdmin(SessionData sessionData) {
        return sessionData.getCurrentUserName().equals("Admin")
            || sessionData.getCurrentUserName().equals("Administrator");
    }

    // 4
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new Point(12, 3, 5), new Point(2, 4, 6));
        space.drawLine(new Point(2, 4, 6), new Point(0, 1, 0));
    }

    class Point {
        public Point(int i, int j, int k) {}
    }

    // 5
    public int calculateWeeklyPay(int hoursWorked, boolean overtime) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = overtime ? 1.5 * hourRate : 1.0 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }

    @Test
    public void calculateWeeklyPayOldMethod() {
        assertThat(calculateWeeklyPay(39, true), is(195));
        assertThat(calculateWeeklyPay(41, true), is(208));

        assertThat(calculateWeeklyPay(39, false), is(195));
        assertThat(calculateWeeklyPay(41, false), is(205));
    }

    @Test
    public void calculatesStraightWeeklyPay() {
        assertThat(calculateStraightWeeklyPay(39), is(195));
        assertThat(calculateStraightWeeklyPay(41), is(205));
    }

    @Test
    public void calculatesWeeklyPayWithOvertime() {
        assertThat(calculateWeeklyPayWithOvertime(39), is(195));
        assertThat(calculateWeeklyPayWithOvertime(41), is(208));
    }

    private Integer calculateStraightWeeklyPay(int hoursWorked) {
        return hoursWorked * hourRate;
    }

    private Integer calculateWeeklyPayWithOvertime(int hoursWorked) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);

        double overtimeRate = 1.5 * hourRate;

        int straightPay = straightTime * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);

        return straightPay + overtimePay;
    }

    ////////////////////////////////////////////////////////////////////////////

    // Abiväljad ja abimeetodid.
    // Need on siin lihtsalt selleks, et kood kompileeruks


    private String item1 = "1";
    private String item2 = "2";
    private String item3 = "3";
    private String item4 = "4";
    private int hourRate = 5;
    int count = 0;
    private List<Order> orders = new ArrayList<Order>();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        nextCount();
        collectFilledOrdersTo(null);
    }

    class Customer {

        public Object getEmployeeCode() {
            // TODO Auto-generated method stub
            return null;
        }

        public Object getType() {
            // TODO Auto-generated method stub
            return null;
        }}

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(Point start, Point end) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }
        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Shape {

        public int y;
        public int x;
        public int curvature;
        public Object height;
        public Object width;
    }

    double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
