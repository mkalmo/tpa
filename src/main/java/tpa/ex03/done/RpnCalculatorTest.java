package tpa.ex03.done;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Stack;

import org.junit.Test;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInAccumulator() {
        RpnCalculator calc = new RpnCalculator();
        assertThat(calc.getAccumulator(), is(0));
    }

    @Test
    public void accumulatorCanBeSet() {
        RpnCalculator calc = new RpnCalculator();
        calc.setAccumulator(1);
        assertThat(calc.getAccumulator(), is(1));
    }

    @Test
    public void calculatorSupportsEnter() {
        // liitmine oli liiga keeruline, jagasime mitmeks
        // tekkib stack andmestruktuur
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        assertThat(c.stack.get(0), is(1));
        assertThat(c.getAccumulator(), is(1));
    }

    @Test
    public void calculatorSupportsAddition() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSupportsMultiplication() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(3);
        c.multiply();
        assertThat(c.getAccumulator(), is(6));
    }

    @Test
    public void expressionOfDepthTwo() {
        // (1 + 2) * 4 = 12
        // 1 2 + 4 *
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();
        assertThat(c.getAccumulator(), is(12));
    }

    @Test
    public void calculatesFromStringInput() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.calculate("1 2 + 4 *"), is(12));
        assertThat(c.calculate("4 3 + 2 1 + *"), is(21));

    }
}

class RpnCalculator {

    Stack<Integer> stack = new Stack<>();

    public void setAccumulator(int i) {
        if (stack.size() > 0) {
            stack.pop();
        }

        stack.push(i);
    }

    public int getAccumulator() {
        return stack.size() > 0 ? stack.peek() : 0;
    }

    public void multiply() {
        stack.push(stack.pop() * stack.pop());
    }

    public void plus() {
        stack.push(stack.pop() + stack.pop());
    }

    public void enter() {
        stack.push(getAccumulator());
    }

    public int calculate(String string) {
        for (String elem : string.split(" ")) {

            if ("+".equals(elem)) {
                plus();
            } else if ("*".equals(elem)) {
                multiply();
            } else {
                enter();
                setAccumulator(Integer.parseInt(elem));
            }
        }

        return getAccumulator();
    }
}