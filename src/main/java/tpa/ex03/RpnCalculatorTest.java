package tpa.ex03;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("unused")
public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInAccumulator() {
        // ...
    }

    @Test
    public void accumulatorCanBeSet() {
        // Tehke võimalikult lihtne lahendus - hoidke seda Integer väljal.
    }

    @Test
    public void calculatorSupportsEnter() {
        // Liitmine on liiga keeruline, jagame mitmeks.
        // Tekkib stack andmestruktuur.
    }

    @Test
    public void calculatorSupportsAddition() {
        // ...
    }

    class RpnCalculator {

        public void setAccumulator(int i) {
            throw new IllegalStateException("not implemented");
        }

        public void plus() {
            throw new IllegalStateException("not implemented");
        }

        public void enter() {
            throw new IllegalStateException("not implemented");
        }

        public int getAccumulator() {
            throw new IllegalStateException("not implemented");
        }
    }
}
