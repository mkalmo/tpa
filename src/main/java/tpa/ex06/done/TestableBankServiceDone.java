package tpa.ex06.done;

import java.util.*;

import tpa.common.*;

public class TestableBankServiceDone implements BankService {

    private List<?> withdrawArguments;
    private List<?> depositArguments;
    private boolean fundsAvailable = true;

    private static Map<String, Double> rates = new HashMap<>();

    static {
        rates.put("EURSEK", 1.0/10);
    }

    public boolean wasWithdrawCalledWith(Money money, String account) {
        return Arrays.asList(money, account).equals(withdrawArguments);
    }

    public boolean wasDepositCalledWith(Money money, String account) {
        return Arrays.asList(money, account).equals(depositArguments);
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        withdrawArguments = Arrays.asList(money, fromAccount);
    }

    @Override
    public void deposit(Money money, String toAccount) {
        depositArguments = Arrays.asList(money, toAccount);
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = rates.get(money.getCurrency() + targetCurrency);

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return fundsAvailable;
    }

    public void setSufficentFundsAvailable(boolean areFundsAvailable) {
        fundsAvailable = false;
    }

    public boolean wasWithdrawCalled() {
        return withdrawArguments != null;
    }

    public void addRate(String fromCurrency, String toCurrency, Double rate) {
        rates.put(fromCurrency + toCurrency, rate);
    }
}