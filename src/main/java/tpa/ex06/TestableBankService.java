package tpa.ex06;

import tpa.common.BankService;
import tpa.common.Money;

public class TestableBankService implements BankService {

    public boolean wasWithdrawCalledWith(Money money, String account) {

        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega

        throw new IllegalStateException("not implemented");
    }

    public boolean wasDepositCalledWith(Money money, String account) {

        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega

        throw new IllegalStateException("not implemented");
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("withdraw: " + money + " - " + fromAccount);

        // siin peaks argumendid meelde jätma

        throw new IllegalStateException("not implemented");
    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("deposit: " + money + " - " + toAccount);

        // siin peaks argumendid meelde jätma

        throw new IllegalStateException("not implemented");
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return true;
    }

    public void setSufficentFundsAvailable(boolean areFundsAvailable) {
    }

    public boolean wasWithdrawCalled() {
        throw new IllegalStateException("not implemented");
    }

    public void addRate(String fromCurrency, String toCurrency, Double rate) {
        throw new IllegalStateException("not implemented");
    }

}