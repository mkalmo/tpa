package tpa.ex06;

import static org.junit.Assert.*;

import org.junit.Test;

import tpa.common.*;

public class TransferServiceTest {

    @Test
    public void transferWithCurrencyConversion() {

        TestableBankService bankService = new TestableBankService();
        TransferService transferService = new TransferService(bankService);

        // E_123 konto valuuta on EUR
        // S_456 konto valuuta on SEK

        // kanda 1 EUR kontolt E_123 kontole S_456
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");


        assertTrue(bankService.wasWithdrawCalledWith(new Money(1, "EUR"), "E_123"));
        assertFalse(bankService.wasWithdrawCalledWith(null, null));

        assertTrue(bankService.wasDepositCalledWith(new Money(10, "SEK"), "S_456"));
        assertFalse(bankService.wasDepositCalledWith(null, null));
    }

    @Test
    public void transferWhenNoFounds() {

        TestableBankService bankService = new TestableBankService();
        bankService.setSufficentFundsAvailable(false);

        TransferService transferService = new TransferService(bankService);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        assertFalse(bankService.wasWithdrawCalled());
    }

    @Test
    public void testWithVisibleRate() {
        TestableBankService bankService = new TestableBankService();
        TransferService transferService = new TransferService(bankService);
        bankService.addRate("EUR", "SEK", 1.0/2); // rate is visible

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        assertTrue(bankService.wasDepositCalledWith(new Money(2, "SEK"), "S_456"));
    }

}