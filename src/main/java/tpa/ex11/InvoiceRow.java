package tpa.ex11;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;

public class InvoiceRow {

    public final BigDecimal amount;
    public final Date date;

    public InvoiceRow(BigDecimal amount, Date date) {
        this.amount = amount;
        this.date = date;
    }

    @Override
    public String toString() {
        return MessageFormat.format("amount: {0}; date: {1}", amount, date);
    }
}