package tpa.ex11.done;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;

import static tpa.util.TestUtil.*;

import java.math.BigDecimal;
import java.util.*;

import org.hamcrest.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import tpa.ex11.InoviceRowGenerator;
import tpa.ex11.InvoiceRow;
import tpa.ex11.InvoiceRowDao;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTestDone {

    @Mock InvoiceRowDao dao;

    @InjectMocks InoviceRowGenerator generator;

    InOrder inOrder;

    @Before
    public void setUp() {
        inOrder = inOrder(dao);
    }

    @Test
    public void amounts_noPaymentsWhenAmountIsZero() {
        generateRowsFor(0, dividedBetweenTwoRows());

        verifyAmounts();
    }

    @Test
    public void amounts_oneRowWhenAmountIsLessThanMinimum() {
        generateRowsFor(2, dividedBetweenTwoRows());

        verifyAmounts(2);
    }

    @Test
    public void amounts_dividesWithoutRemainder() {
        generateRowsFor(6, dividedBetweenTwoRows());

        verifyAmounts(3, 3);
    }

    @Test
    public void amounts_dividesWithRemainder() {
        generateRowsFor(11, dividedBetweenThreeRows());

        verifyAmounts(3, 3, 5);
    }

    @Test
    public void amounts_amountWouldBeLessThanMinimum() {
        generateRowsFor(8, dividedBetweenThreeRows());

        verifyAmounts(4, 4);
    }

    @Test
    public void dates_periodStartsAndEndsInTheBeginningOfMonth() {
        generator.generateRowsFor(
                new BigDecimal(10), asDate("2014-01-01"),
                                    asDate("2014-03-01"));

        inOrder.verify(dao).save(rowWithDate("2014-01-01"));
        inOrder.verify(dao).save(rowWithDate("2014-02-01"));
        inOrder.verify(dao).save(rowWithDate("2014-03-01"));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void dates_periodStartsAndEndsInTheMiddleOfMonth() {
        generator.generateRowsFor(
                new BigDecimal(6), asDate("2014-01-15"),
                                   asDate("2014-02-15"));

        inOrder.verify(dao).save(rowWithDate("2014-01-15"));
        inOrder.verify(dao).save(rowWithDate("2014-02-01"));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void dates_amountIsNotEnoughForWholePeriod() {
        generator.generateRowsFor(
                new BigDecimal(3), asDate("2014-01-01"),
                                   asDate("2014-02-01"));

        inOrder.verify(dao).save(rowWithDate("2014-01-01"));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountMatcherTest() {
        InvoiceRow row = new InvoiceRow(BigDecimal.ONE, null);
        assertThat(row, amountMatcher(1));
        assertThat(row, not(amountMatcher(0)));
    }

    @Test
    public void dateMatcherTest() {
        InvoiceRow row = new InvoiceRow(null, asDate("2014-01-01"));
        assertThat(row, dateMatcher("2014-01-01"));
        assertThat(row, not(dateMatcher("2014-01-02")));
    }

    private InvoiceRow rowWithDate(final String date) {
        return argThat(dateMatcher(date));
    }

    private void verifyAmounts(int ... amounts) {
        for (int amount : amounts) {
            inOrder.verify(dao, calls(1)).save(argThat(amountMatcher(amount)));
        }

        verifyNoMoreInteractions(dao);
    }

    private void generateRowsFor(int amount, List<Date> interval) {
        generator.generateRowsFor(
                new BigDecimal(amount), interval.get(0), interval.get(1));
    }

    private List<Date> dividedBetweenTwoRows() {
        return Arrays.asList(asDate("2014-01-01"), asDate("2014-02-01"));
    }

    private List<Date> dividedBetweenThreeRows() {
        return Arrays.asList(asDate("2014-01-01"), asDate("2014-03-01"));
    }

    private Matcher<InvoiceRow> dateMatcher(final String storedDate) {
        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return asDate(storedDate).equals(item.date);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(storedDate);
            }
        };
    }

    private Matcher<InvoiceRow> amountMatcher(final Integer storedAmount) {
        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return storedAmount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(storedAmount));
            }
        };
    }
}