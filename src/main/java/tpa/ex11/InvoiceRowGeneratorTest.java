package tpa.ex11;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;
import static tpa.util.TestUtil.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.*;

import org.hamcrest.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTest {

    @Mock InvoiceRowDao dao;

    @InjectMocks InoviceRowGenerator generator;

    InOrder inOrder;

    @Before
    public void setUp() {
        inOrder = inOrder(dao);
    }

    @Test
    public void amounts_noPaymentsWhenAmountIsZero() {
        generator.generateRowsFor(
                new BigDecimal(0), asDate("2014-01-01"), asDate("2014-02-01"));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amounts_oneRowWhenAmountIsLessThanMinimum() {
        generator.generateRowsFor(
                new BigDecimal(2), asDate("2014-01-01"), asDate("2014-02-01"));

        inOrder.verify(dao).save(argThat(amountMatcher(2)));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amounts_dividesWithoutRemainder() {
        generator.generateRowsFor(
                new BigDecimal(6), asDate("2014-01-01"), asDate("2014-02-01"));

        inOrder.verify(dao, times(2)).save(argThat(amountMatcher(3)));

        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amounts_dividesWithRemainder() {
        // 11 kolmeks -> [3, 3, 5]
    }

    @Test
    public void amounts_amountWouldBeLessThanMinimum() {
        // 8 kolmeks -> [4, 4]
    }

    @Test
    public void dates_periodStartsAndEndsInTheBeginningOfMonth() {
        generator.generateRowsFor(
                new BigDecimal(10), asDate("2014-01-01"),
                                    asDate("2014-03-01"));

        inOrder.verify(dao).save(rowWithDate("2014-01-01"));
        inOrder.verify(dao).save(rowWithDate("2014-02-01"));
        inOrder.verify(dao).save(rowWithDate("2014-03-01"));

        verifyNoMoreInteractions(dao);
    }

    private InvoiceRow rowWithDate(final String date) {
        return argThat(dateMatcher(date));
    }

    private Matcher<InvoiceRow> dateMatcher(final String storedDate) {
        // kirjutage vastav matcher
        return null;
    }

    private Matcher<InvoiceRow> amountMatcher(final Integer storedAmount) {
        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return storedAmount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(storedAmount));
            }
        };
    }
}
