package tpa.ex11;

import java.math.*;
import java.text.MessageFormat;
import java.util.*;

public class InoviceRowGenerator {

    public static final BigDecimal minimumPaymentAmount = new BigDecimal(3);

    private InvoiceRowDao dao;

    public void generateRowsFor(BigDecimal amount, Date periodStart, Date periodEnd) {

        if (periodEnd.before(periodStart)) throw new IllegalArgumentException();

        if (amount.compareTo(BigDecimal.ZERO) == 0) {
            // no payments
            return;
        }

        if (amount.compareTo(minimumPaymentAmount) < 0) {
            Payment first = paymentsForDates(periodStart, periodEnd, amount).get(0);
            first.amount = amount;
            savePayments(Arrays.asList(first));
            // just one payment with the whole amount
            return;
        }

        List<Payment> payments = paymentsForDates(periodStart, periodEnd, amount);
        BigDecimal remainder = amount.remainder(new BigDecimal(payments.size()));

        if (remainder.compareTo(BigDecimal.ZERO) == 0) {
            divideBetweenPayments(amount, payments);
        } else {
            divideBetweenPayments(amount, payments);
            Payment last = payments.get(payments.size() - 1);
            last.amount = last.amount.add(remainder);
        }

        savePayments(payments);
    }

    private void divideBetweenPayments(BigDecimal amount, List<Payment> payments) {
        for (Payment payment : payments) {
            payment.amount = onePaymentAmount(amount, payments);
        }
    }

    private void savePayments(List<Payment> payments) {
        for (Payment payment : payments) {
            dao.save(new InvoiceRow(payment.amount, payment.date));
        }
    }

    private BigDecimal onePaymentAmount(BigDecimal amount, List<Payment> payments) {
        BigDecimal paymentCount = new BigDecimal(payments.size());
        return amount.divide(paymentCount, RoundingMode.FLOOR);
    }

    private List<Payment> paymentsForDates(Date periodStart, Date periodEnd, BigDecimal amount) {
        List<Payment> payments = new ArrayList<>();

        Date runner = periodStart;
        do {
            payments.add(new Payment(runner));
            runner = getBeginningOfNextMonth(runner);
            amount = amount.subtract(minimumPaymentAmount);

            if (amount.compareTo(minimumPaymentAmount) < 0) {
                break;
            }

        } while (!runner.after(periodEnd));

        return payments;
    }

    private Date getBeginningOfNextMonth(Date date) {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(date);

        do {
            c.add(Calendar.DAY_OF_MONTH, 1);
        } while (c.get(Calendar.DAY_OF_MONTH) != 1);

        return c.getTime();
    }

    private class Payment {
        public Payment(Date date) {
            this.date = date;
        }
        Date date;
        BigDecimal amount;
        @Override
        public String toString() {
            return MessageFormat.format("{0} - {1}", date, amount);
        }
    }
}
