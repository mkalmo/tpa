package tpa.ex10.person;

import java.util.List;
import java.util.stream.Collectors;

public class PersonUtil {

    public Person getOldest(List<Person> persons) {
        ensureNotNull(persons);
        ensureNotEmpty(persons);

        Person oldestPerson = persons.get(0);
        for (Person person : persons) {
            if (person.getAge() > oldestPerson.getAge()) {
                oldestPerson = person;
            }
        }

        return oldestPerson;
    }

    public List<Person> getPersonsInLegalAge(List<Person> persons) {
        ensureNotNull(persons);

        return persons.stream()
            .filter(person -> person.getAge() >= Person.LEGAL_AGE)
            .collect(Collectors.toList());
    }

    public List<Person> getWomen(List<Person> persons) {
        ensureNotNull(persons);

        return persons.stream()
                .filter(person -> Person.GENDER_FEMALE.equals(person.getGender()))
                .collect(Collectors.toList());
    }

    public List<Person> getPersonsWhoLiveIn(String town, List<Person> persons) {
        ensureNotNull(persons);
        ensureNotNull(town);

        return persons.stream()
                .filter(person -> town.equals(person.getAddress().getTown()))
                .collect(Collectors.toList());
    }

    private void ensureNotEmpty(List<Person> persons) {
        if (persons.isEmpty()) throw new IllegalArgumentException();
    }

    private void ensureNotNull(Object object) {
        if (object == null) throw new IllegalArgumentException();
    }
}