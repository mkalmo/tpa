package tpa.ex10.done;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.*;

import org.junit.Test;

import tpa.ex09.DataSource;
import tpa.ex09.LineItem;
import tpa.ex09.Order;
import tpa.ex10.order.OrderService;
import static tpa.util.TestUtil.*;

public class OrderServiceTestDone {

    @Test
    public void shouldFindFilledOrders() {
        OrderService orderService = orderService()
                .add(anOrder().withNumber("A"))
                .add(anOrder().withNumber("B").isFilled())
                .build();

        List<Order> filledOrders = orderService.getFilledOrders();

        assertThat(filledOrders.size(), is(1));
        assertThat(filledOrders.get(0).getNumber(), is("B"));
    }

    @Test
    public void shouldFindOrdersOverOverCertainAmount() {
        OrderService orderService = orderService()
                .add(anOrder().withItem("pen", 1))
                .add(anOrder().withItem("desk", 30))
                .build();

        List<Order> orders = orderService.getOrdersOver(10);

        assertThat(orders.size(), is(1));
        assertThat(orders.get(0).getTotal(), is(30.0));
    }

    @Test
    public void shouldFindOrdersSortedByDate() {
        OrderService orderService = orderService()
                .add(anOrder().withDate("2014-01-01"))
                .add(anOrder().withDate("2013-01-01"))
                .build();

        List<Order> orders = orderService.getOrdersSortedByDate();

        assertThat(orders.size(), is(2));
        assertThat(orders.get(0).getOrderDate(), is(asDate("2013-01-01")));
        assertThat(orders.get(1).getOrderDate(), is(asDate("2014-01-01")));
    }

    private OrderServiceBuilder orderService() {
        return new OrderServiceBuilder();
    }

    private OrderBuilder anOrder() {
        return new OrderBuilder();
    }
}

class OrderServiceBuilder {

    List<Order> orders = new ArrayList<Order>();

    public OrderServiceBuilder add(OrderBuilder orderBuilder) {
        orders.add(orderBuilder.build());
        return this;
    }

    public OrderService build() {
        return new OrderService(new DataSource() {
            @Override
            public List<Order> getOrders() {
                return orders;
            }
        });
    }
}

class OrderBuilder {

    private String number;
    private Date date = new Date();
    private boolean filled = false;

    List<LineItem> lineItems = new ArrayList<LineItem>();

    public OrderBuilder withNumber(String number) {
        this.number = number;
        return this;
    }

    public OrderBuilder withDate(String dateAsString) {
        this.date = asDate(dateAsString);
        return this;
    }

    public OrderBuilder withItem(String name, double price) {
        lineItems.add(new LineItem(name, price, 1));
        return this;
    }

    public OrderBuilder isFilled() {
        this.filled = true;
        return this;
    }

    public Order build() {
        Order order = new Order(number, date);
        for (LineItem lineItem : lineItems) {
            order.addItem(lineItem);
        }
        order.setFilled(filled);
        return order;
    }
}
