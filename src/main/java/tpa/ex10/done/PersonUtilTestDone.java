package tpa.ex10.done;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static java.util.Arrays.*;

import java.util.Arrays;

import org.junit.Test;

import tpa.ex10.person.Address;
import tpa.ex10.person.Person;
import tpa.ex10.person.PersonUtil;
import static com.googlecode.catchexception.CatchException.*;

public class PersonUtilTestDone {

    private PersonUtil personUtil = new PersonUtil();

    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
    }

    @Test
    public void findsPersonsInLegalAge() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(17).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)),
                   containsInAnyOrder(p1, p2));
    }

    @Test
    public void findsWomen() {
        Person p1 = aPerson().howIsWoman().build();
        Person p2 = aPerson().howIsMan().build();
        Person p3 = aPerson().howIsMan().build();

        assertThat(personUtil.getWomen(asList(p1, p2, p3)), contains(p1));
    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().livingIn("Tallinn").build();
        Person p2 = aPerson().livingIn("Tartu").build();
        Person p3 = aPerson().livingIn("Narva").build();

        assertThat(
                personUtil.getPersonsWhoLiveIn("Tallinn", asList(p1, p2, p3)),
                contains(p1));
    }

    @Test
    public void gettingOldestFromEmptyListThrows() {
        verifyException(personUtil, IllegalArgumentException.class)
            .getOldest(Arrays.<Person>asList());
    }

    @Test
    public void nullThrows() {

        verifyException(personUtil, IllegalArgumentException.class)
            .getOldest(null);

        verifyException(personUtil, IllegalArgumentException.class)
            .getPersonsInLegalAge(null);

        verifyException(personUtil,IllegalArgumentException.class)
        .getWomen(null);

        verifyException(personUtil, IllegalArgumentException.class)
            .getPersonsWhoLiveIn("Tallinn", null);

        verifyException(personUtil, IllegalArgumentException.class)
            .getPersonsWhoLiveIn(null, Arrays.<Person>asList());
    }

    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}

class PersonBuilder {

    private Integer age = 20;
    private String name = "John";
    private String gender = Person.GENDER_MALE;
    private Address address = new Address("Kase 10", "Tallinn");

    public PersonBuilder withAge(Integer age) {
        this.age = age;
        return this;
    }

    public PersonBuilder livingIn(String town) {
        address.setTown(town);
        return this;
    }

    public PersonBuilder howIsWoman() {
        this.gender = Person.GENDER_FEMALE;
        return this;
    }

    public PersonBuilder howIsMan() {
        this.gender = Person.GENDER_MALE;
        return this;
    }

    public Person build() {
        Person person = new Person(name, age, gender, address);
        return person;
    }
}