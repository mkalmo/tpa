package tpa.ex07.done;

import static org.mockito.Mockito.*;

import org.junit.Test;

import tpa.common.BankService;
import tpa.common.TransferService;
import tpa.common.Money;

public class TransferServiceTestMockitoDone {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.convert(new Money(1, "EUR"), "EUR")).thenReturn(new Money(1, "EUR"));
        when(bankService.convert(new Money(1, "EUR"), "SEK")).thenReturn(new Money(10, "SEK"));
        when(bankService.hasSufficientFundsFor(anyMoney(), anyAccount())).thenReturn(true);


        // kanda 1 EUR kontolt E_123 kontole S_456
        // E_123 konto valuuta on EUR
        // S_456 konto valuuta on SEK
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");


        verify(bankService).withdraw(new Money(1, "EUR"), "E_123");
        verify(bankService).deposit(new Money(10, "SEK"), "S_456");
    }

    @Test
    public void transferWhenNotEnoughFunds() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.convert(new Money(1, "EUR"), "EUR")).thenReturn(new Money(1, "EUR"));
        when(bankService.hasSufficientFundsFor(anyMoney(), anyAccount())).thenReturn(false);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService, never()).withdraw(anyMoney(), anyAccount());
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }

}