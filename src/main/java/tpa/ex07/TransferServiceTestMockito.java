package tpa.ex07;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mockito;

import tpa.common.BankService;
import tpa.common.TransferService;
import tpa.common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        // when(...
        // seda mida on vaja õpetada leiab transferService.transfer() meetodist.

        // kanda 1 EUR kontolt E_123 kontole S_456
        // E_123 konto valuuta on EUR
        // S_456 konto valuuta on SEK
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService).withdraw(new Money(1, "EUR"), "E_123");
        // verify(bankService).deposit(...
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        // ...
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}