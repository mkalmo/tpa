package tpa.ex07;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mockito;

import tpa.common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockitoSimple {

    // BankService ja TransferService on siinsamas failis allpool

    @Test
    public void transferSuccessScenario() {
        // ...
    }

    @Test
    public void transferingNegativeAmountFails() {
        // ...
    }

    @Test
    public void transferFailsWhenNotEnoughFunds() {
        // ...
    }

}

interface BankService {

    int getBalance(String formAccount);

    void transfer(int amount, String formAccount, String toAccount);

}

class TransferService {

    private BankService bankService;

    public TransferService(BankService bankService) {
        this.bankService = bankService;
    }

    public void transferMoney(int amount, String formAccount, String toAccount) {

        if (amount <= 0 || formAccount.equals(toAccount))
            return;

        if (bankService.getBalance(formAccount) >= amount) {
            bankService.transfer(amount, formAccount, toAccount);
        }
    }
}
