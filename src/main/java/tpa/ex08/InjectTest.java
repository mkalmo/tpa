package tpa.ex08;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.laughingpanda.beaninject.Inject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@SuppressWarnings("unused")
public class InjectTest {

    BankService bankService = new BankService();
    TransferService transferService = new TransferService();

    @Test
    public void beanInject() {
        BankService bankService = new BankService();
        TransferService transferService = new TransferService();

        // Inject.bean(...

        assertThat(transferService.getBankService(), is(notNullValue()));
    }

    @Test
    public void mockitoRunner() {
        assertThat(transferService.getBankService(), is(notNullValue()));
    }

}
