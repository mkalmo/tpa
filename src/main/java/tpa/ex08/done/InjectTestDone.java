package tpa.ex08.done;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.laughingpanda.beaninject.Inject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import tpa.ex08.*;

@RunWith(MockitoJUnitRunner.class)
public class InjectTestDone {

    @Mock BankService bankService = new BankService();
    @InjectMocks TransferService transferService = new TransferService();

    @Test
    public void beanInject() {
        BankService bankService = new BankService();
        TransferService transferService = new TransferService();

        Inject.bean(transferService).with(bankService);

        assertThat(transferService.getBankService(), is(notNullValue()));
    }

    @Test
    public void mockitoRunner() {
        assertThat(transferService.getBankService(), is(notNullValue()));
    }

}
