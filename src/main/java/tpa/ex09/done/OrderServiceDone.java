package tpa.ex09.done;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import tpa.ex09.*;

@SuppressWarnings("unused")
public class OrderServiceDone {

    private DataSource dataSource;

    public OrderServiceDone(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        return dataSource.getOrders().stream()
                .filter(order -> order.isFilled())
                .collect(Collectors.toList());
    }

    public List<Order> getOrdersOver(double amount) {
        return dataSource.getOrders().stream()
                .filter(order -> order.getTotal() > amount)
                .collect(Collectors.toList());
    }

    public List<Order> getOrdersSortedByDate() {
        return dataSource.getOrders().stream()
            .sorted(getOrderDateComparator())
            .collect(Collectors.toList());
    }

    private Comparator<Order> getOrderDateComparator() {
        return (Order o1, Order o2) ->
            o1.getOrderDate().compareTo(o2.getOrderDate());
    }
}
