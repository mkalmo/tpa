package tpa.ex09;

import java.util.List;


public interface DataSource {

	List<Order> getOrders();

}
