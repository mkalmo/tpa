Ülesanne L1

Kirjutage translaator, mis tõlgib araabia numbrid rooma numbriteks.
Sisend: 2008, Väljund: MMVIII.
Sisendiks võivad olla numbrid 1-3000.

Rooma numbrid

I: 1
V: 5
X: 10
L: 50
C: 100
D: 500
M: 1000

Suvalised kombinatsioonid pole lubatud. Näiteks IM ei ole sobilik tähistama 999-t.
Igatüvenumbrit tuleb käsitleda eraldi, alustades vasakust ja jättes vahele nullid.

Näide

1990: 1000=M, 900=CM, 90=XC; ehk MCMXC.
2008: 2000=MM, 8=VIII; ehk MMVIII.

Lisa

kirjutage teist pidi translaator.
