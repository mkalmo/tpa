package tpa.exL1;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.Map;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class RomanNumeralsTest {

    Translator c = new Translator();

    @Test
    @Parameters({"1, I", "5, V", "10, X", "50, L", "500, D", "1000, M"})
    public void translatesExactMatches(int arabic, String roman) {
        assertThat(c.toRoman(arabic), is(roman));
    }

    @Test
    public void translatesSimpleNotExactMatches() {
        assertThat(c.toRoman(2), is("II"));
        assertThat(c.toRoman(3), is("III"));
        assertThat(c.toRoman(4), is("IV"));
        assertThat(c.toRoman(6), is("VI"));
        assertThat(c.toRoman(7), is("VII"));
        assertThat(c.toRoman(8), is("VIII"));
        assertThat(c.toRoman(9), is("IX"));
        assertThat(c.toRoman(11), is("XI"));
    }

    @Test
    public void translatesMoreComplexCases() {
        assertThat(c.toRoman(42), is("XLII"));
        assertThat(c.toRoman(999), is("CMXCIX"));
        assertThat(c.toRoman(1990), is("MCMXC"));
        assertThat(c.toRoman(2014), is("MMXIV"));
    }

    @Test
    public void doesReverseTranslates() {
        assertThat(c.toArabic("CMXCIX"), is(999));
    }

}

class Translator {

    private Map<Integer, String> map = new LinkedHashMap<Integer, String>();

    public Translator() {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    }

    public String toRoman(Integer target) {
        String component = findLargestMatch(target);
        int componentValue = getComponentValue(component);
        if (componentValue < target) {
            return component + toRoman(target - componentValue);
        } else {
            return component;
        }
    }

    private int getComponentValue(String roman) {
        for (Integer key : map.keySet()) {
            if (map.get(key).equals(roman)) {
                return key;
            }
        }

        throw new IllegalStateException("no entry for: " + roman);
    }

    private String findLargestMatch(Integer number) {
        for (Integer key : map.keySet()) {
            if (key <= number) {
                return map.get(key);
            }
        }

        throw new IllegalStateException("error for " + number);
    }

    public Integer toArabic(String romanNumeral) {
        for (int i = 1; i <= 3000; i++) {
            if (romanNumeral.equals(toRoman(i))) {
                return i;
            }
        }

        throw new IllegalArgumentException("only numbers 1-3000 are allowed");
    }


}