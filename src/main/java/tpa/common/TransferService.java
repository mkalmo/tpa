package tpa.common;


public class TransferService {

    private BankService bankService;

    public TransferService(BankService bankService) {
        this.bankService = bankService;
    }

    public void transfer(Money money, String fromAccount, String toAccount) {

        // konverteeri summa vähendatava konto valuutasse
        // ja vähenda kontot. Kui sisendvaluuta juhtub sama olema,
        // mis väljundvaluuta, siis tagastatakse ka sama summa.

        String fromCurrency = bankService.getAccountCurrency(fromAccount);
        Money withdrawAmountConverted = bankService.convert(money, fromCurrency);

        if (!bankService.hasSufficientFundsFor(withdrawAmountConverted, fromAccount)) {
            return;
        }

        bankService.withdraw(withdrawAmountConverted, fromAccount);

        // konverteeri summa suurendatava konto valuutasse
        // ja suurenda kontot

        String toCurrency = bankService.getAccountCurrency(toAccount);
        Money depositAmountConverted = bankService.convert(money, toCurrency);

        bankService.deposit(depositAmountConverted, toAccount);
    }
}