package tpa.ex07b;

import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class TicketSellerTest {

    private Receiver smsIn = mock(Receiver.class);
    private Receiver ticket = mock(Receiver.class);
    private Receiver bill = mock(Receiver.class);
    private Receiver smsOut = mock(Receiver.class);

    @Test
    public void buyTicketWithSmsAllMocked() throws Exception {
        Hub hub = new Hub();
        hub.addModule("smsIn", smsIn);
        hub.addModule("ticket", ticket);
        hub.addModule("bill", bill);
        hub.addModule("smsOut", smsOut);

        doAnswer(sendMessage(hub, "sms:buy")).when(smsIn).message("buy");
        doAnswer(sendMessage(hub, "sms:bought")).when(ticket).message("sms:buy");
        doAnswer(sendMessage(hub, "sms:billed")).when(bill).message("sms:bill");

        smsIn.message("buy");

        verify(smsOut).message("sendSms");
    }

    @Test
    public void buyTicketWithSmsUsingRealBill() throws Exception {
        Hub hub = new Hub();
        hub.addModule("smsIn", smsIn);
        hub.addModule("ticket", ticket);
        hub.addModule("bill", new Bill(hub));
        hub.addModule("smsOut", smsOut);

        doAnswer(sendMessage(hub, "sms:buy")).when(smsIn).message("buy");
        doAnswer(sendMessage(hub, "sms:bought")).when(ticket).message("sms:buy");

        smsIn.message("buy");

        verify(smsOut).message("sendSms");
    }

    class Bill implements Receiver {

        private Hub hub;

        public Bill(Hub hub) {
            this.hub = hub;
        }

        @Override
        public void message(String message) {
            System.out.println("bill got message: " + message);
            hub.message("sms:billed:..");
        }

    }

    private Answer<Object> sendMessage(Hub c, String message) {
        return new SentMessageAction(c, message);
    }

    class SentMessageAction implements Answer<Object> {

        private String message;
        private Hub hub;

        public SentMessageAction(Hub hub, String message) {
            this.message = message;
            this.hub = hub;
        }

        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            hub.message(message);
            return null;
        }

    }

}
