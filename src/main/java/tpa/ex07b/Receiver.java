package tpa.ex07b;

interface Receiver {
    void message(String message);
}
