package tpa.exL2;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class GameOfLifeTest {

    @Test
    public void frameKnowsWhichSquersAreAlive() {
        Frame frame = getFrame("--X",
                               "-X-");

        assertThat(frame.isAlive(p(0, 0)), is(false));
        assertThat(frame.isAlive(p(1, 0)), is(false));
        assertThat(frame.isAlive(p(2, 0)), is(true));

        assertThat(frame.isAlive(p(0, 1)), is(false));
        assertThat(frame.isAlive(p(1, 1)), is(true));
        assertThat(frame.isAlive(p(2, 1)), is(false));
    }

    @Test
    public void frameKnowsNeigborCount() {
        Frame frame = getFrame("XXX",
                               "-X-",
                               "--X");

        assertThat(frame.getNeighbourCount(p(0, 0)), is(2));
        assertThat(frame.getNeighbourCount(p(1, 0)), is(3));
        assertThat(frame.getNeighbourCount(p(2, 0)), is(2));

        assertThat(frame.getNeighbourCount(p(0, 1)), is(3));
        assertThat(frame.getNeighbourCount(p(1, 1)), is(4));
        assertThat(frame.getNeighbourCount(p(2, 1)), is(4));

        assertThat(frame.getNeighbourCount(p(0, 2)), is(1));
        assertThat(frame.getNeighbourCount(p(1, 2)), is(2));
        assertThat(frame.getNeighbourCount(p(2, 2)), is(1));
    }

    @Test
    public void frameHasToString() {
        Frame frame = getFrame("XXX",
                               "-X-",
                               "--X");

        assertThat(frame.toString(), is("XXX\n-X-\n--X\n"));
    }

    @Test
    public void gameKnowsTheRules() {

        Game game = new Game(getFrame(""));
        boolean alive = true;
        boolean dead = false;

        // rule 1
        assertThat(game.getNextState(alive, 0), is(dead));

        // rule 2
        assertThat(game.getNextState(alive, 2), is(alive));
        assertThat(game.getNextState(alive, 3), is(alive));

        // rule 3
        assertThat(game.getNextState(alive, 4), is(dead));

        // rule 4
        assertThat(game.getNextState(dead, 3), is(alive));
    }

    @Test
    public void frameKnowsItsPoints() {
        Frame frame = getFrame("-",
                               "-");

        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 1);

        assertThat(frame.getPoints(), is(Arrays.asList(p1, p2)));
    }

    @Test
    public void frameKnowsItsHightAndWidht() {
        Frame frame = getFrame("---",
                               "---");

        assertThat(frame.getHeight(), is(2));
        assertThat(frame.getWidth(), is(3));
    }

    @Test
    public void framePointsCanBeMarkedAlive() {
        Frame frame = getFrame("--");

        frame.markAlive(p(0, 0));

        assertThat(frame.isAlive(p(0, 0)), is(true));
        assertThat(frame.isAlive(p(1, 0)), is(false));

    }

    @Test
    public void stillWorksCorrectly() {
        Frame frame = getFrame("------",
                               "--XX--",
                               "-X--X-",
                               "--XX--",
                               "------");


        Game game = new Game(frame);

        assertThat(game.nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void pulsarWorksCorrectly() {
        Frame frame = getFrame("------",
                               "-XX---",
                               "-X----",
                               "----X-",
                               "---XX-",
                               "------");

        Frame expected = getFrame("------",
                                  "-XX---",
                                  "-XX---",
                                  "---XX-",
                                  "---XX-",
                                  "------");

        Game game = new Game(frame);

        assertThat(game.nextFrame(), is(equalTo(expected)));
    }

    @Test
    public void gliderWorksCorrectly() {
        Frame frame1 = getFrame("-X----",
                                "--XX--",
                                "-XX---",
                                "------");

        Frame frame2 = getFrame("--X---",
                                "---X--",
                                "-XXX--",
                                "------");

        Frame frame3 = getFrame("------",
                                "-X-X--",
                                "--XX--",
                                "--X---");

        Game game = new Game(frame1);

        assertThat(game.nextFrame(), is(equalTo(frame2)));
        assertThat(game.nextFrame(), is(equalTo(frame3)));
    }

    @Test
    public void gameCanCalculateTheNextFrame() {
        Frame frame1 = getFrame("XX-",
                                "X--",
                                "---",
                                "--X");

        Frame frame2 = getFrame("XX-",
                                "XX-",
                                "---",
                                "---");

        Game game = new Game(frame1);

        assertThat(game.nextFrame(), is(equalTo(frame2)));
    }

    private Point p(int x, int y) {
        return new Point(x, y);
    }

    private Frame getFrame(String ... rows) {
        Frame frame = new Frame(rows[0].length(), rows.length);
        for (Point p : frame.getPoints()) {
            String isAlive = String.valueOf(rows[p.y].charAt(p.x));
            if (Frame.ALIVE.equals(isAlive)) {
                frame.markAlive(p);
            }
        }

        return frame;
    }
}

class Frame {

    public final static String ALIVE = "X";
    public final static String DEAD = "-";

    private boolean[][] matrix;

    public Frame(int width, int height) {
        matrix = new boolean[height][width];
    }

    public List<Point> getPoints() {
        List<Point> points = new ArrayList<Point>();
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                points.add(new Point(x, y));
            }
        }

        return points;
    }

    public Integer getWidth() {
        return matrix[0].length;
    }

    public Integer getHeight() {
        return matrix.length;
    }

    @Override
    public String toString() {
        String retval = "";
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                retval += matrix[y][x] ? ALIVE : DEAD;
            }
            retval += "\n";
        }

        return retval;
    }

    @Override
    public boolean equals(Object obj) {
        Frame other = (Frame) obj;
        if (!other.getPoints().equals(getPoints())) {
            return false;
        }

        for (Point p : getPoints()) {
            if (other.isAlive(p) != isAlive(p)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public Integer getNeighbourCount(Point p) {
        int minX = Math.max(0, p.x - 1);
        int maxX = Math.min(p.x + 1, getWidth() - 1);
        int minY = Math.max(0, p.y - 1);
        int maxY = Math.min(p.y + 1, getHeight() - 1);
        int count = 0;
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                if (x == p.x && y == p.y) {
                    continue;
                }
                if (matrix[y][x]) {
                    count++;
                }
            }
        }

        return count;
    }

    public boolean isAlive(Point p) {
        return matrix[p.y][p.x];
    }

    public void markAlive(Point point) {
        matrix[point.y][point.x] = true;
    }
}

class Game {

    private Frame frame;

    public Game(Frame frame) {
        this.frame = frame;
    }

    public boolean getNextState(boolean isAlive, int neighbourCount) {

        if (isAlive && neighbourCount < 2) {
            return false;
        } else if (isAlive && neighbourCount == 2 || neighbourCount == 3) {
            return true;
        } else if (isAlive && neighbourCount > 3) {
            return false;
        } else if (!isAlive && neighbourCount == 3) {
            return true;
        } else if (!isAlive && neighbourCount != 3) {
            return false;
        } else {
            throw new IllegalStateException(
                    MessageFormat.format("no rule for alive({0}), count({1})",
                            isAlive, neighbourCount));
        }
    }

    public Frame nextFrame() {
        Frame nextFrame = new Frame(frame.getWidth(), frame.getHeight());
        for (Point p : frame.getPoints()) {
            boolean isAlive = frame.isAlive(p);
            int neighbourCount = frame.getNeighbourCount(p);
            if (getNextState(isAlive, neighbourCount)) {
                nextFrame.markAlive(p);
            }
        }

        this.frame = nextFrame;

        return nextFrame;
    }

}

class Point {

    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return MessageFormat.format("p({0}, {1})", x, y);
    }

    @Override
    public boolean equals(Object obj) {
        Point other = (Point) obj;
        return other.x == x && other.y == y;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}