package tpa.ex02.done;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = getStack();
        assertThat(stack.getSize(), is(0));
    }

    private Stack getStack() {
        return new Stack(100);
    }

    @Test
    public void pushIncreasesSize() {
        Stack stack = getStack();
        stack.push(1);
        assertThat(stack.getSize(), is(1));
    }

    @Test
    public void popDecreasesSize() {
        Stack stack = getStack();
        stack.push(1);
        stack.pop();
        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void pushedElementsCanBeRetrieved() {
        Stack stack = getStack();
        stack.push(1);
        assertThat(stack.pop(), is(1));
    }

    @Test
    public void lastElementInIsFirstOut() {
        Stack stack = getStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);

        assertThat(stack.pop(), is(3));
        assertThat(stack.pop(), is(2));
        assertThat(stack.pop(), is(1));
    }

    @Test
    public void peekReturnsLastElement() {
        Stack stack = getStack();
        stack.push(1);
        assertThat(stack.peek(), is(1));
    }

    @Test
    public void peekDoesNotRemoveElements() {
        Stack stack = getStack();
        stack.push(1);
        assertThat(stack.getSize(), is(1));
    }

    @Test(expected = IllegalStateException.class)
    public void popingEmptyStackThrows() {
        Stack stack = getStack();
        stack.pop();
    }

    @Test(expected = IllegalStateException.class)
    public void peekingEmptyStackThrows() {
        Stack stack = getStack();
        stack.peek();
    }
}