package tpa.ex02.done;

public class Stack {

    private int[] stack;

    private int size = 0;

    public Stack(int size) {
         stack = new int[size];
    }

    public int pop() {
        ensureStackIsNotEmpty();
        return stack[--size];
    }

    public int peek() {
        ensureStackIsNotEmpty();
        return stack[size - 1];
    }

    private void ensureStackIsNotEmpty() {
        if (size < 1) {
            throw new IllegalStateException("stack is empty");
        }
    }

    public void push(int i) {
        stack[size++] = i;
    }

    public int getSize() {
        return size;
    }
}